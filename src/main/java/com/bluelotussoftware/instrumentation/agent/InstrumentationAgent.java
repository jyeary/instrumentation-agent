/*
 * Copyright 2012-2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.instrumentation.agent;

import java.lang.instrument.Instrumentation;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This provides a
 * <code>javaagent</code> instrumentation for the VM. This class provides
 * convenience methods for getting the classes loaded by the VM.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class InstrumentationAgent {

    private static final Logger log = Logger.getLogger(InstrumentationAgent.class.getName());
    private static Instrumentation instrumentation;

    /**
     * This method loads the agent prior to invoking the main method.
     *
     * @param agentArgs Arguments to be passed to the agent.
     * @param inst Instrumentation (agent).
     * @throws Exception if any {@code Exception} occurs while calling the
     * method and creating the agent.
     */
    public static void premain(String agentArgs, Instrumentation inst) throws Exception {
        log.log(Level.INFO, "premain() method called with agentArgs {0} and inst {1}", new Object[]{agentArgs, inst.getClass()});
        InstrumentationAgent.instrumentation = inst;
    }

    /**
     * This method is used to set the agent on the main method after the JVM is
     * already running.
     *
     * @param agentArgs Arguments to be passed to the agent.
     * @param inst Instrumentation (agent).
     */
    public static void agentmain(String agentArgs, Instrumentation inst) {
        log.log(Level.INFO, "agentmain() method called with agentArgs {0} and inst {1}", new Object[]{agentArgs, inst.getClass()});
        InstrumentationAgent.instrumentation = inst;
    }

    /**
     * This method returns the wrapped instrument (agent).
     *
     * @return The wrapped agent.
     */
    public static Instrumentation getInstrumentation() {
        return instrumentation;
    }

    /**
     * Returns an array of all classes currently loaded by the JVM.
     *
     * @return an array containing all the classes loaded by the JVM,
     * zero-length if there are none.
     */
    public static Class[] getAllLoadedClasses() {
        return instrumentation.getAllLoadedClasses();
    }

    /**
     * Returns an array of all classes for which
     * {@link ClassLoader#getSystemClassLoader()} is an initiating loader.
     *
     * @return an array containing all the classes for which
     * {@link ClassLoader#getSystemClassLoader()} is an initiating loader,
     * zero-length if there are none.
     */
    public static Class[] getSystemClassLoaderInitiatedClasses() {
        return instrumentation.getInitiatedClasses(ClassLoader.getSystemClassLoader());
    }

    /**
     * Returns an array of all classes for which loader is an initiating loader.
     * If the supplied loader is null, classes initiated by the bootstrap class
     * loader are returned.
     *
     * @param classLoader the loader whose initiated class list will be
     * returned.
     * @return an array containing all the classes for which loader is an
     * initiating loader, zero-length if there are none
     */
    public static Class[] getClassLoaderInitiatedClasses(final ClassLoader classLoader) {
        return instrumentation.getInitiatedClasses(classLoader);
    }

    /**
     * Static initialization method to load the {@link Instrumentation} if it
     * has not already been loaded.
     */
    public static void initialize() {
        log.log(Level.INFO, "initialize() method called.");
        if (instrumentation == null) {
            log.log(Level.INFO, "Instrumentation was null calling AgentLoader.");
            AgentLoader.loadAgent();
        }
    }
}
