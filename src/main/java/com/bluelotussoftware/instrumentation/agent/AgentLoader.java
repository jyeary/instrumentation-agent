/*
 * Copyright 2012-2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.instrumentation.agent;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;
import java.io.IOException;
import java.lang.management.ManagementFactory;

/**
 *
 * This class is responsible for dynamically loading the instrumentation agent
 * jar.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
public class AgentLoader {

    /**
     * This method returns the path to the {@link Instrumentation} agent located
     * in the .m2 repository.
     *
     * @return Path to the agent jar.
     */
    private static String getAgentPath() {
        return new StringBuilder().append(System.getProperty("user.home"))
                .append("/.m2/repository/com/bluelotussoftware/instrumentation-agent/1.0/instrumentation-agent-1.0.jar")
                .toString();
    }

    /**
     * Static method for loading an agent into the currently running JVM.
     */
    public static void loadAgent() {
        String runtimeMXBeanName = ManagementFactory.getRuntimeMXBean().getName();
        int endIndex = runtimeMXBeanName.indexOf('@');
        String pid = runtimeMXBeanName.substring(0, endIndex);

        try {
            VirtualMachine vm = VirtualMachine.attach(pid);
            vm.loadAgent(getAgentPath());
            vm.detach();
        } catch (AttachNotSupportedException | IOException | AgentLoadException | AgentInitializationException e) {
            throw new RuntimeException(e);
        }
    }
}